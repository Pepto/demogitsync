---
title: 'Let''s Encrypt installation'
media_order: certbot-logo-1A.svg
---

L'installation présentée est pour Ubuntu 18.04 et 16.04 LTS, mais l'installation sur autre chose n'est pas plus compliqué.

## **Prérequis** 
* Avoir les privilèges Sudo.
* Un nom de domaine enregistré et qui pointe à votre adress IP public du serveur.


![](certbot-logo-1A.svg)


## **Certbot**
**Première étape** est d'installer le **client** L'ets Encrypt (cerbot-auto) et le sauvegarder dans (/usr/sbin)
