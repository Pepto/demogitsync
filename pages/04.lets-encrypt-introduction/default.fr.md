---
title: 'Let''s Encrypt introduction'
media_order: 'letsencrypt-log.png,Let''s_Encrypt_Certificate_Firefox_example.png'
---

## **Introduction**
Ceci est une **documentation** pour indiquer ce qu'est **Let's Encrypt** et donner les informations necessaires pour qu'une autre personne puisse s'en servir (Ce n'est pas une ducumentation qui va dans les gros détails). Let's Encrypt est une autorité de certification. ( une **[autorité de certification](https://fr.wikipedia.org/wiki/Autorit%C3%A9_de_certification)** délivre des certificats pour décrire des identités numérique et donne les moyens de vérifier leurs certificats) Ce sont des certificats **X.509**, pour le protocole de cryptographie **TLS**, gratuits. En fin août, Let's Encrypt à dépassé les **50 millions** de noms de domaine sécurisés.                          ![](letsencrypt-log.png)

## **Sponsors**
Plusieurs compagnies sponsorisent ce service.
* [La Fondation Mozilla](https://fr.wikipedia.org/wiki/Mozilla_Foundation)
* [Cisco Systems](https://fr.wikipedia.org/wiki/Cisco_Systems)
* [La Fondation Linux](https://fr.wikipedia.org/wiki/Fondation_Linux)
* [L'université du Michigan](https://fr.wikipedia.org/wiki/Universit%C3%A9_du_Michigan)
* [L'etreprise Free](https://fr.wikipedia.org/wiki/Free_(entreprise))
* _Etc._


[plugin:youtube](https://www.youtube.com/watch?v=ksqTu7TX83g)

## **Le but?**
Faire une **généralisation** de l'usage des connexions sécurisées, **supprimer la necessité de payer** et de
rendre le tout **automatisé** pour ce passer de la version actuel qui est manuel.
* La **création**
* La **validation**
* La **signature**
* L'**installation**
* Le **renouvellement des certificats**


## **Comment?**
Le protocol  **_défi-réponse_**([challenge-response](https://en.wikipedia.org/wiki/Challenge%E2%80%93response_authentication)) utilisé pour rendre le tout** automatique** est appelé **_Automated Certificate Management Environment_**. Il fonctionne en envoyant plusieurs requêtes au serveur web qui est couvert par le certificat et la validation par le domaine est fait en vérifiant les réponses reçus. Le certificat crée un **serveur TLS spécial**, sur le serveur système, et il va recevoir des requêtes spécials envoyé par l'extension _Server Name Indication_ de **TLS**. La validation est faite plusieurs fois sur plusieurs chemins réseaux et les attaques d'usurpation DNS sont rendu plus difficile grace à la vérification des entrées faites a partir de plusieurs points géographique.

![](Let's_Encrypt_Certificate_Firefox_example.png)
Par Mozilla Foundation and contributorsScreenshot:Vulphere — Self-taken; derivative work, MPL 2, https://commons.wikimedia.org/w/index.php?curid=74622152
 
## **La complexité ? très faible!**
Il suffit que de quelque commandes indiqués sur la page d'installation.
[Installation](http://192.168.99.100:8001/lets-encrypt-installation)


